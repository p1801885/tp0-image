# TP0 : Analyse

## Binome

* Sherine Baya KOCHKAR, 12018503
* Badreddine OUAR, 11801885

Lien Gitlab: https://forge.univ-lyon1.fr/p1801885/tp0-image

## II - A. Normalisation d'histogramme

Voici un comparatif Avant-Après de deux images différentes et de leur histogramme sous l'effet de la normalisation

L'ensemble des histogrammes sont affichés via une fonction `calc_hist` écrite manuellement.

L'opération de normalisation est effectuée par la fonction `histogram_normalization`

<img src="captures/1.png">

Figure 1: Caméraman - Image originale et image normalisée

<img src="captures/2.png">

Figure 2: Caméraman - Histogrammes image originale et image normalisée

<img src="captures/9.png">

Figure 3: Léna - Image originale et image normalisée

<img src="captures/10.png">

Figure 4: Léna - Histogrammes image originale et image normalisée

## II - B. Etirement d'histogramme

Avant et après l'étirement. Les calculs sont faits dans la fonction `histogram_stretching`

L'étirement s'est réalisé avec les valeurs `a = 50` et `b = 200`.

<img src="captures/3.png">

Figure 5: Caméraman - Image originale et image étirée

<img src="captures/4.png">

Figure 6: Caméraman - Histogrammes image originale et image étirée

<img src="captures/11.png">

Figure 7: Léna - Image originale et image étirée

<img src="captures/12.png">

Figure 8: Léna - Histogrammes image originale et image étirée

## II - C. Egalisation d'histogramme

Avant et après l'égalisation. Les calculs sont faits dans la fonction `histogram_equalization`

<img src="captures/5.png">

Figure 9: Caméraman - Image originale et image égalisée

<img src="captures/6.png">

Figure 10: Caméraman - Histogrammes image originale et image égalisée

<img src="captures/13.png">

Figure 11: Léna - Image originale et image égalisée

<img src="captures/14.png">

Figure 12: Léna - Histogrammes image originale et image égalisée

## III. Filtrage

L'algorithme de filtrage est géré par les fonctions `apply_filter`, `convolve` et `calculate_convolution`. 

Le filtre générique 3x3 est donné par les valeurs: `[[1,2,1],[0,0,0],[-1,-2,-1]]`

<img src="captures/7.png">

Figure 13: Caméraman - Image originale et image filtrée 

<img src="captures/8.png">

Figure 14: Léna - Image originale et image filtrée
