import cv2
import matplotlib.pyplot as plt

camera = 'cameraman.sousexp.png'
lena = 'lena.png'

def calc_hist(image):

    hist = [0] * 256
    for row in image:
        for pixel_value in row:
            hist[pixel_value] += 1
    abscisse = []
    for i in range(256):
        abscisse.append(i)
    
    return abscisse, hist

# Fonction pour la normalisation d'histogramme
def histogram_normalization(image_path):
    # Charger l'image en niveaux de gris
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

    # Calcul de l'histogramme

    abscisse, hist = calc_hist(image)

    # Calcul des probabilités d'occurrence
    total_pixels = image.size
    hist_prob = [count / total_pixels for count in hist]

    # Calcul de la fonction de distribution cumulative
    cum_sum = 0
    cum_sum_list = []
    for prob in hist_prob:
        cum_sum += prob
        cum_sum_list.append(cum_sum)

    # Normalisation dans l'intervalle [0, 255]
    normalized_values = [int(255 * val) for val in cum_sum_list]

    # Application de la normalisation à l'image
    normalized_image = [[normalized_values[pixel] for pixel in row] for row in image]

    hist_norm = calc_hist(normalized_image)[1]

    # Affichage des images avant et après la normalisation
    plt.subplot(1, 2, 1)
    plt.imshow(image, cmap='gray')
    plt.title('Image originale')

    plt.subplot(1, 2, 2)
    plt.imshow(normalized_image, cmap='gray')
    plt.title('Image normalisée')

    plt.show()

    # Affichage des histogrammes avant et après la normalisation
    plt.subplot(1, 2, 1)
    plt.bar(abscisse,hist)
    plt.title('Hist original')

    plt.subplot(1, 2, 2)
    plt.bar(abscisse,hist_norm)
    plt.title('Hist normalisé')

    plt.show()

# Exemple d'utilisation
histogram_normalization(lena)

# Fonction pour l'étirement d'histogramme
def histogram_stretching(image_path, a, b):
    # Charger l'image en niveaux de gris
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    abscisse, hist = calc_hist(image)

    # Trouver les valeurs minimales et maximales dans l'image
    min_val = min([min(row) for row in image])
    max_val = max([max(row) for row in image])

    # Appliquer la transformation d'étirement d'histogramme
    stretched_image = [[int((b - a) * (pixel - min_val) / (max_val - min_val) + a) for pixel in row] for row in image]
    hist_stretch = calc_hist(stretched_image)[1]

    # Affichage des images avant et après l'étirement d'histogramme
    plt.subplot(1, 2, 1)
    plt.imshow(image, cmap='gray')
    plt.title('Image originale')

    plt.subplot(1, 2, 2)
    plt.imshow(stretched_image, cmap='gray')
    plt.title('Image étirée d\'histogramme')

    plt.show()

    # Affichage des histogrammes avant et après l'étirement
    plt.subplot(1, 2, 1)
    plt.bar(abscisse,hist)
    plt.title('Hist original')

    plt.subplot(1, 2, 2)
    plt.bar(abscisse,hist_stretch)
    plt.title('Hist étiré')

    plt.show()

# Exemple d'utilisation
histogram_stretching(lena, 50, 200)

# Fonction pour l'égalisation d'histogramme
def histogram_equalization(image_path):
    # Charger l'image en niveaux de gris
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

    # Calcul de l'histogramme
    abscisse, hist = calc_hist(image)

    # Calcul de l'histogramme cumulé
    cumulative_hist = 0
    cumulative_hist_list = []
    for count in hist:
        cumulative_hist += count
        cumulative_hist_list.append(cumulative_hist)

    # Normalisation de l'histogramme cumulé dans l'intervalle [0, 1]
    normalized_cumulative_hist = [cumulative / cumulative_hist for cumulative in cumulative_hist_list]

    # Application de la transformation d'égalisation d'histogramme
    equalized_values = [[int(255 * normalized_cumulative_hist[pixel]) for pixel in row] for row in image]
    hist_eq = calc_hist(equalized_values)[1]

    # Affichage des images avant et après l'égalisation d'histogramme
    plt.subplot(1, 2, 1)
    plt.imshow(image, cmap='gray')
    plt.title('Image originale')

    plt.subplot(1, 2, 2)
    plt.imshow(equalized_values, cmap='gray')
    plt.title('Image égalisée d\'histogramme')

    plt.show()

    # Affichage des histogrammes avant et après l'égalisation
    plt.subplot(1, 2, 1)
    plt.bar(abscisse,hist)
    plt.title('Hist original')

    plt.subplot(1, 2, 2)
    plt.bar(abscisse,hist_eq)
    plt.title('Hist égalisé')

    plt.show()

histogram_equalization(lena)

# Fonction pour appliquer un filtre
def apply_filter(image_path, kernel):
    # Charger l'image en niveaux de gris
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    abscisse, hist = calc_hist(image)

    # Appliquer le filtre à l'image
    filtered_image = convolve(image, kernel)

    # Affichage de l'image originale et de l'image filtrée
    plt.subplot(1, 2, 1)
    plt.imshow(image, cmap='gray')
    plt.title('Image originale')

    plt.subplot(1, 2, 2)
    plt.imshow(filtered_image, cmap='gray')
    plt.title('Image filtrée')

    plt.show()

    # Affichage des histogrammes avant et après le filtrage

# Fonction pour effectuer la convolution sans utiliser numpy
def convolve(image, kernel):
    rows, cols = len(image), len(image[0])
    k_rows, k_cols = len(kernel), len(kernel[0])
    result = [[0] * cols for _ in range(rows)]

    for i in range(rows):
        for j in range(cols):
            value = calculate_convolution(image, kernel, i, j, k_rows, k_cols)
            result[i][j] = value

    return result

# Fonction pour calculer la convolution d'un pixel
def calculate_convolution(image, kernel, i, j, k_rows, k_cols):
    value = 0
    for x in range(k_rows):
        for y in range(k_cols):
            row, col = i - k_rows // 2 + x, j - k_cols // 2 + y
            if 0 <= row < len(image) and 0 <= col < len(image[0]):
                value += image[row][col] * kernel[x][y]
    return value

# Exemple d'utilisation
# Définir des valeurs pour le filtre générique 3x3
f11, f12, f13 = 1, 2, 1
f21, f22, f23 = 0, 0, 0
f31, f32, f33 = -1, -2, -1

# Créer le filtre générique 3x3
custom_filter = [
    [f11, f12, f13],
    [f21, f22, f23],
    [f31, f32, f33]
]

# Appliquer le filtre à l'image
apply_filter(lena, custom_filter)
